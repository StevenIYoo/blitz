defmodule  Blitz.Application do
  use Application
  require Logger

  alias Blitz.RiotAPI

  def start(_type, _args) do

    children = []

    opts = [strategy: :one_for_one, name: Blitz.Supervisor]

    {:ok, pid} = Supervisor.start_link(children, opts)

    {:ok, summoner_name} = Application.fetch_env(:blitz, :summoner_name)
    {:ok, platform} = Application.fetch_env(:blitz, :platform)
    start_application(summoner_name, platform)

    {:ok, pid}
  end

  @doc """
  Gets summoner information and
  then starts up monitoring for new matches every minute for the next hour
  Will also monitor the last people played last 5 matches
  """
  @spec start_application(String.t(), String.t()) :: :ok
  def start_application(name, platform) do
    region = RiotAPI.get_region_by_platform(platform)

    with {:ok, summoner} <- RiotAPI.get_summoner_info(name, platform),
         {:ok, matches} = RiotAPI.get_matches(summoner.puuid, region) do

      summoners = matches
        |> Enum.reduce([], fn match_id, summoners ->
          [RiotAPI.get_participants_from_match(match_id, region) | summoners]
        end)

      summoners = summoners
        |> Enum.concat
        |> Enum.uniq
        |> Enum.take(10)


      summoners
        |> Enum.each(fn summoner -> BlitzServer.start_link({summoner.name, summoner.puuid, platform}) end)

      summoners = summoners
        |> Enum.map(fn summoner -> summoner.name end)
        |> Enum.join(", ")

      IO.puts("[#{summoners}]")
    else
      _ -> IO.puts("unexpected errors")
    end
  end
end
